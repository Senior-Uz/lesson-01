package main

import (
	"fmt"
)

func main() {
	var r float32 = 10.04

	// fmt.Scanf("%f", &r)
	fmt.Println("R =", r)

	fmt.Printf("Area: %0.2f\n", area(r))
}

func area(r float32) (area float32) {
	
	var Pi float32 = 3.14159265359

	// Pr2
	corner := 4 * r * r 
	circle := Pi * r*r

	area = corner - circle

	return area
}
